<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
// Type
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ChangeEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('emailTemp', EmailType::class, array(
                'label' => false,
                'attr' => array('placeholder' => 'Nouvel email', 'class' => 'form-control')
            ))
            ->add('oldPassword', PasswordType::class, array(
                'constraints' => array(
                    new UserPassword(array('message' => 'Le mot de passe ne correspond pas.')),
                ),
                'label' => false,
                'attr' => array('placeholder' => 'Mot de passe', 'class' => 'form-control'),
            ))
            ->add('Valider', SubmitType::class, array('attr' => array('class' => 'btn btn_orange center_block')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}