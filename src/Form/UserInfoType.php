<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
// Type
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UserInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profilPic', FileType::class, ['label' => 'Image de profil', 'data_class' => null, 'required' => false, 'attr' => array('onchange' => 'preview_image(event)')])
            ->add('pseudo', TextType::class, ['required' => false,'attr' => array('class' => 'form-control')])
            ->add('twitch', TextType::class, ['required' => false,'attr' => array('class' => 'form-control')])
            ->add('discord', TextType::class, ['required' => false,'attr' => array('class' => 'form-control')])
            ->add('youtube', TextType::class, ['required' => false,'attr' => array('class' => 'form-control')])
            ->add('bio', TextareaType ::class, ['required' => false,'attr' => array('class' => 'form-control', 'style' =>  'height: 263px; resize: none;')])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}