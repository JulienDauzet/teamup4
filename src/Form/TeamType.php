<?php

namespace App\Form;

use App\Entity\Team;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
// Type
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class TeamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Nom de la structure', 'attr' => array('placeholder' => 'Nom de votre structure ', 'autocomplete' => 'disabled', 'class' => 'form-control')])
            ->add('tag', TextType::class, ['label' => 'Tag de team (10 caractères maximum) ', 'attr' => array('placeholder' => 'TeamUP', 'autocomplete' => 'disabled', 'class' => 'form-control')])
            ->add('bio', TextareaType::class, ['label' => 'À propos', 'attr' => array('placeholder' => 'Notre équipe vise les sommets et au delà...', 'autocomplete' => 'disabled', 'class' => 'form-control', 'style' =>  'height: 203px; resize: none;')])
            ->add('banner', FileType::class, ['label' => false, 'data_class' => null, 'required' => false, 'attr' => array('onchange' => 'preview_banner(event)')])
            ->add('logo', FileType::class, ['label' => false, 'data_class' => null, 'required' => false, 'attr' => array('onchange' => 'preview_image(event)')])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Team::class,
        ));
    }
}