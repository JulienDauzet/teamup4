<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EgoGameType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('game_name', TextType::class, [
                'required' => true,
                'attr' => ['class' => 'select-form-control-100']
            ])
            ->add('ownNote', ChoiceType::class, [
                'required' => true,
                'attr' => ['class' => 'select-form-control-100'],
                'choices' => ['0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5],
            ])
            ->add('game_id', HiddenType::class, [
                'required' => true
            ])
            ->add('image', HiddenType::class)
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\EgoGame'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_egoGame';
    }


}
