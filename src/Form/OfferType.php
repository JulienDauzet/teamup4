<?php

namespace App\Form;

use App\Entity\Offer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
// Type
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'Intitulé du poste','attr' => array('placeholder' => 'Support, Frontliner, Heal, etc...', 'autocomplete' => 'disabled', 'class' => 'form-control')])
            ->add('text', TextareaType::class, ['label' => 'Description du poste','attr' => array('placeholder' => 'Les qualités recherchées par votre recrutement...', 'autocomplete' => 'disabled', 'class' => 'form-control', 'style' =>  'height: 263px; resize: none;')])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Offer::class,
        ));
    }
}