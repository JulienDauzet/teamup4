<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
// Type
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
// Constraints
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Doctrine\DBAL\Schema\Constraint;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', PasswordType::class, array(
                'constraints' => array(
                    new UserPassword(array('message' => 'Le mot de passe actuel est érroné.')),
                ),
                'label' => false,
                'attr' => array('placeholder' => 'Ancien mot de passe', 'class' => 'form-control'),
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => false, 'attr' => array('placeholder' => 'Nouveau mot de passe', 'class' => 'form-control')),
                'second_options' => array('label' => false, 'attr' => array('placeholder' => 'Confirmer le nouveau mot de passe', 'class' => 'form-control')),
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('max' => 4096))
                ),                
            ))
            ->add('Valider', SubmitType::class, array('attr' => array('class' => 'btn btn_orange center_block')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}