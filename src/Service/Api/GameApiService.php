<?php

namespace App\Service\Api;

use App\Model\Cover;
use App\Model\Game;
use App\Model\System;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GameApiService
{

    private $client;

    const MAX_ALLOWED_RESULTS = 50; // Nombre de resultats max autorisés en 1 requete
    private $apiKey; // Dans config/services.yml

    /**
     * GameApiService constructor.
     * @param String $apiKey: Chargé depuis services.yml
     * Initialisation du client de l'api
     */
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;

        $this->client = new Client([
            'base_uri' => 'https://api-v3.igdb.com/',
        ]);
    }

    /****************************************************************/

    /**
     * Retourne un objet Game complet depuis son id
     * @param int $id
     * @return Game
     */
    public function getFullGameById($id)
    {
        $gameJson = $this->getGameByIdApi($id);
        $game = $this->jsonToGame($gameJson, true);

        return $game;
    }

    /**
     * Retourne un objet Game avec uniquement les champs id et name depuis son id
     * @param int $id
     * @return Game
     */
    public function getGameById($id)
    {
        $gameJson = $this->getGameByIdApi($id);
        $game = $this->jsonToGame($gameJson, false);

        return $game;
    }

    /**
     * Retourne un objet System depuis son id
     * @param int $id
     * @return System
     */
    public function getSystemById($id)
    {
        $systemJson = $this->getSystemByIdApi($id);
        $system = $this->jsonToSystem($systemJson);

        return $system;
    }

    /**
     * Retourne un objet Cover depuis son id
     * @param int $id
     * @return Cover
     */
    public function getCoverById($id)
    {
        $coverJson = $this->getCoverByIdApi($id);
        $cover = $this->jsonToCover($coverJson);

        return $cover;
    }

    /**
     * Retourne un array de noms de jeux correspondant à la query
     * @param $query
     * @param int $nbResults
     * @return String[]
     */
    public function getGameNamesByQuery($query, $nbResults = 10)
    {
        $gamesRaw = $this->getGameNameByNameApi($query, $nbResults);

        $names = [];

        foreach ($gamesRaw as $game){
            array_push($names, $game['name']);
        }
        return $names;
    }

    /****************************************************************/

    /**
     * Retourne un array d'objets Game en fonction de la query
     * @param String $gameName
     * @param int $maxResults
     * @param bool $fullGame True pour avoir les objets Game complets (Cover et Systems), False pour uniquement les noms
     * @return Game[]
     */
    public function getGameListByName($gameName, $maxResults = self::MAX_ALLOWED_RESULTS, $fullGame = false)
    {
        if ($maxResults > self::MAX_ALLOWED_RESULTS || $maxResults <= 0) {
            $maxResults = self::MAX_ALLOWED_RESULTS;
        }

        $gamesRaw = $this->getGameListByNameApi($gameName, $maxResults);

        $games = [];

        foreach ($gamesRaw as $game)
        {
            array_push($games, $fullGame ? $this->getFullGameById($game['id']) : $this->getGameById($game['id']));
        }

        return $games;
    }

    /****************************************************************/
    /**
     * Retourne les uniquement le nom des jeux correspondants à la query
     * @param String $gameName
     * @param int $maxResults
     * @return mixed
     */
    public function getGameNameByNameApi($gameName, $maxResults = self::MAX_ALLOWED_RESULTS)
    {
        if ($maxResults > self::MAX_ALLOWED_RESULTS || $maxResults <= 0) {
            $maxResults = self::MAX_ALLOWED_RESULTS;
        }

        $response = null;

        try {
            $response = $this->client
                ->request('GET', 'games', [
                    'headers' => [
                        'Accept' => 'application/json',
                        "user-key" => $this->apiKey,
                    ],
                    'body' => 'fields name,popularity,platforms,cover;where name ~*"'.$gameName.'"*; limit '.$maxResults.'; sort popularity desc;',
                ])
                ->getBody()
                ->getContents()
            ;
        } catch (GuzzleException $e) {
            echo("Erreur : getGameNameByNameApi");
            echo($e->getMessage());
        }

        return json_decode($response, true);
    }
    /****************************************************************/

    /**
     * Retourne une liste d'id de Game correspondant à la query en interrogeant l'api
     * @param String $gameName
     * @param int $maxResults
     * @return array
     */
    public function getGameListByNameApi($gameName, $maxResults)
    {
        $response = null;

        try {
            $response = $this->client
                ->request('GET', 'games', [
                    'headers' => [
                        'Accept' => 'application/json',
                        "user-key" => $this->apiKey,
                    ],
                    'body' => 'fields id,popularity;where name ~*"'.$gameName.'"*; limit '.$maxResults.'; sort popularity desc;',
                ])
                ->getBody()
                ->getContents()
            ;
        } catch (GuzzleException $e) {
            echo("Erreur : getGameListByNameApi");
            echo($e->getMessage());
        }

        return json_decode($response, true);
    }

    /**
     * Return un jeu en depuis son id en interrogeant l'api
     * @param int $id
     * @return mixed
     */
    public function getGameByIdApi($id)
    {
        $response = null;

        try {
            $response = $this->client
                ->request('GET', 'games', [
                    'headers' => [
                        'Accept' => 'application/json',
                        "user-key" => $this->apiKey,
                    ],
                    'body' => 'fields id,name,cover,platforms;where id='.$id.';',
                ])
                ->getBody()
                ->getContents()
            ;
        } catch (GuzzleException $e) {
            echo("Erreur : getGameByIdApi");
            echo($e->getMessage());
        }

        return json_decode($response, true);
    }

    /**
     * Return un system en depuis son id en interrogeant l'api
     * @param int $id
     * @return String
     */
    public function getSystemByIdApi($id)
    {
        $response = null;

        try {
            $response = $this->client
                ->request('GET', 'platforms', [
                    'headers' => [
                        'Accept' => 'application/json',
                        "user-key" => $this->apiKey,
                    ],
                    'body' => 'fields id, name,abbreviation,alternative_name; where id = '.$id.';',
                ])
                ->getBody()
                ->getContents()
            ;
        } catch (GuzzleException $e) {
            echo("Erreur : getSystemByIdApi");
            echo($e->getMessage());
        }

        return json_decode($response, true);
    }

    /**
     * Return une cover en depuis son id en interrogeant l'api
     * @param int $id
     * @return mixed
     */
    public function getCoverByIdApi($id)
    {
        $response = null;

        try {
            $response = $this->client
                ->request('GET', 'covers', [
                    'headers' => [
                        'Accept' => 'application/json',
                        "user-key" => $this->apiKey,
                    ],
                    'body' => 'fields id,url;where id='.$id.';',
                ])
                ->getBody()
                ->getContents()
            ;
        } catch (GuzzleException $e) {
            echo("Erreur : getCoverByIdApi");
            echo($e->getMessage());
        }

        return json_decode($response, true);
    }

    /****************************************************************/

    /**
     * Transforme les données json de l'api en objet Game
     * @param String $json
     * @param bool $full: Si true, l'objet Game chargera aussi Systems et Cover
     * @return Game
     */
    private function jsonToGame($json, $full = true)
    {
        $game = new Game();
        $game
            ->setId($json[0]['id'])
            ->setName($json[0]['name']);

        if (isset($json[0]['cover']) && $full)
        {
            $game->setCoverId($this->getCoverById($json[0]['cover']));
        }

        if ($full)
        {
            $systems = [];

            if (isset($json[0]['platforms'])) {

                foreach ($json[0]['platforms'] as $systemId)
                {
                    array_push($systems, $this->getSystemById($systemId));
                }
            }

            $game->setSystems($systems);
        }

        return $game;
    }

    /**
     * Transforme les données json de l'api en objet System
     * @param String $json
     * @return System
     */
    private function jsonToSystem($json)
    {
        $system = new System();
        $system
            ->setId($json[0]['id'])
            ->setName($json[0]['name'])
            ->setAbbreviationName($json[0]['abbreviation']);

        return $system;
    }

    /**
     * Transforme les données json de l'api en objet Cover
     * @param String $json
     * @return Cover
     */
    private function jsonToCover($json)
    {
        $cover = new Cover();
        $cover
            ->setId($json[0]['id'])
            ->setUrl($json[0]['url']);

        return $cover;
    }

    /****************************************************************/

}