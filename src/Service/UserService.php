<?php

namespace App\Service;


use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;


class UserService
{
    public $user;
    public $userRepository;
    private $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->security = $security;
        $this->user = $this->security->getUser();
        $this->userRepository = $em->getRepository(User::class);
    }
    

    public function isUserTeamed(){

        $notTeamed = $this->userRepository->findNotTeamedArray();

        if(in_array($this->user, $notTeamed)){
            /* 1 si l'utilisateur n'est pas dans une equipe */
            return 1;
        }else{
            return 0;
        }


    }
}
