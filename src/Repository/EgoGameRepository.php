<?php

namespace App\Repository;

use App\Entity\EgoGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EgoGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method EgoGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method EgoGame[]    findAll()
 * @method EgoGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EgoGameRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EgoGame::class);
    }

    // /**
    //  * @return EgoGame[] Returns an array of EgoGame objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EgoGame
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
