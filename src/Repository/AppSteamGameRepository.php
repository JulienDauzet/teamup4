<?php

namespace App\Repository;

use App\Entity\AppSteamGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AppSteamGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppSteamGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppSteamGame[]    findAll()
 * @method AppSteamGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppSteamGameRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AppSteamGame::class);
    }

    // /**
    //  * @return AppSteamGame[] Returns an array of AppSteamGame objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppSteamGame
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
