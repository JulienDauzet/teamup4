<?php

namespace App\Repository;

use App\Entity\PubNote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PubNote|null find($id, $lockMode = null, $lockVersion = null)
 * @method PubNote|null findOneBy(array $criteria, array $orderBy = null)
 * @method PubNote[]    findAll()
 * @method PubNote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PubNoteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PubNote::class);
    }

    // /**
    //  * @return PubNote[] Returns an array of PubNote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PubNote
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
