<?php

namespace App\Model;


class System
{
    private $id;
    private $name;
    private $abbreviationName;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id; return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name; return $this;
    }

    /**
     * @return mixed
     */
    public function getAbbreviationName()
    {
        return $this->abbreviationName;
    }

    /**
     * @param mixed $abbreviationName
     */
    public function setAbbreviationName($abbreviationName)
    {
        $this->abbreviationName = $abbreviationName; return $this;
    }


}