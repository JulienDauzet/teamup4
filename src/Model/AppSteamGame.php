<?php

namespace App\Model;



class AppSteamGame
{

    public $id;

    public $appid;

    public $name;

    public $playtime_forever;

    public $img_icon_url;

    public $img_logo_url;

    public $playtime_forever_mins;


    public $has_community_visible_stats;

    public function __construct(){

    }

    public function convertToHoursMins($time, $format = '%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAppid(): ?int
    {
        return $this->appid;
    }

    public function setAppid(int $appid): self
    {
        $this->appid = $appid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPlaytimeForever(): ?int
    {
        return $this->playtime_forever;
    }

    public function setPlaytimeForever(?int $playtime_forever): self
    {
        $this->playtime_forever = $playtime_forever;

        return $this;
    }

    public function getPlaytimeForeverMins()
    {
        return $this->playtime_forever_mins;
    }

    public function setPlaytimeForeverMins(): self
    {
        $this->playtime_forever_mins = $this->convertToHoursMins($this->playtime_forever);

        return $this;
    }

    public function getImgIconUrl(): ?string
    {
        return $this->img_icon_url;
    }

    public function setImgIconUrl(?string $img_icon_url): self
    {
        $this->img_icon_url = $img_icon_url;

        return $this;
    }

    public function getImgLogoUrl(): ?string
    {
        return $this->img_logo_url;
    }

    public function setImgLogoUrl(?string $img_logo_url): self
    {
        $this->img_logo_url = $img_logo_url;

        return $this;
    }

    public function getHasCommunityVisibleStats(): ?bool
    {
        return $this->has_community_visible_stats;
    }

    public function setHasCommunityVisibleStats(bool $has_community_visible_stats): self
    {
        $this->has_community_visible_stats = $has_community_visible_stats;

        return $this;
    }
}
