<?php

namespace App\Model;


class Game
{

    private $id;
    private $name;
    private $coverId;
    private $systems;

    public function __construct()
    {
    }


    public function getId() { return $this->id; }
    public function setId($id) { $this->id = $id; return $this; }

    public function getName() { return $this->name; }
    public function setName($name) { $this->name = $name; return $this; }

    public function getCoverId() { return $this->coverId; }
    public function setCoverId($coverId) { $this->coverId = $coverId; return $this; }

    public function getSystems() { return $this->systems; }
    public function setSystems($systems) { $this->systems = $systems; return $this; }



}