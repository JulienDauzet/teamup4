<?php

namespace App\Model;


class Cover
{
    private $id;
    private $url;

    public function getId() { return $this->id; }
    public function setId($id) { $this->id = $id; return $this; }

    public function getUrl() { return $this->url; }
    public function setUrl($url) { $this->url = $url; return $this; }



}