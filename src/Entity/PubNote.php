<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PubNoteRepository")
 */
class PubNote
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     * Plusieurs offres sont liées à une lineUp
     * @ORM\ManyToOne(targetEntity="User", inversedBy="pubNotes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Plusieurs offres sont liées à une lineUp
     * @ORM\ManyToOne(targetEntity="EgoGame", inversedBy="pubNotes")
     * @ORM\JoinColumn(name="egogame_id", referencedColumnName="id")
     */
    private $egoGame;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEgoGame(): ?EgoGame
    {
        return $this->egoGame;
    }

    public function setEgoGame(?EgoGame $egoGame): self
    {
        $this->egoGame = $egoGame;

        return $this;
    }
}
