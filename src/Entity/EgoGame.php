<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EgoGameRepository")
 */
class EgoGame
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $game_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $game_id;

    /**
     * Plusieurs offres sont liées à une lineUp
     * @ORM\ManyToOne(targetEntity="User", inversedBy="egoGames")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $ownNote;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="PubNote", mappedBy="egoGame", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $pubNotes;

    public function __construct()
    {
        $this->pubNotes = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGameName(): ?string
    {
        return $this->game_name;
    }

    public function setGameName(string $game_name): self
    {
        $this->game_name = $game_name;

        return $this;
    }

    public function getGameId(): ?int
    {
        return $this->game_id;
    }

    public function setGameId(int $game_id): self
    {
        $this->game_id = $game_id;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getOwnNote(): ?int
    {
        return $this->ownNote;
    }

    public function setOwnNote(int $ownNote): self
    {
        $this->ownNote = $ownNote;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPubNotes()
    {
        return $this->pubNotes;
    }

    public function setPubNotes($pubNotes): self
    {
        $this->pubNotes = $pubNotes;

        return $this;
    }

    public function addPubNote(PubNote $pubNote): self
    {
        if (!$this->pubNotes->contains($pubNote)) {
            $this->pubNotes[] = $pubNote;
            $pubNote->setEgoGame($this);
        }

        return $this;
    }

    public function removePubNote(PubNote $pubNote): self
    {
        if ($this->pubNotes->contains($pubNote)) {
            $this->pubNotes->removeElement($pubNote);
            // set the owning side to null (unless already changed)
            if ($pubNote->getEgoGame() === $this) {
                $pubNote->setEgoGame(null);
            }
        }

        return $this;
    }
}
