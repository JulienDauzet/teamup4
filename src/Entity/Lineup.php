<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Lineup
 *
 * @ORM\Table(name="lineup")
 * @ORM\Entity(repositoryClass="App\Repository\LineupRepository")
 */
class Lineup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Plusieurs offres sont liées à une lineUp
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="lineUps")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    private $team;

    /**
     * @ORM\Column(type="integer")
     */
    private $game_id;

    /**
     *  @ORM\Column(type="string", nullable=true)
     */
    private $game_name;

    /**
     *  @ORM\Column(type="string")
     */
    private $system_id;

    /**
     *  @ORM\Column(type="string")
     */
    private $system_name;

    /**
     *  @ORM\Column(type="string")
     */
    private $image;
    
    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="lineUp", cascade={"all"})
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity="Offer", mappedBy="lineUp", cascade={"persist", "remove"})
     */
    private $offers;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set game
     *
     * @param string $game
     *
     * @return Lineup
     */
    public function setGame($game)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return string
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set team
     *
     * @param string $team
     *
     * @return Lineup
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return string
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set gameId
     *
     * @param integer $gameId
     *
     * @return Lineup
     */
    public function setGameId($gameId)
    {
        $this->game_id = $gameId;

        return $this;
    }

    /**
     * Get gameId
     *
     * @return integer
     */
    public function getGameId()
    {
        return $this->game_id;
    }

    /**
     * Set gameName
     *
     * @param string $gameName
     *
     * @return Lineup
     */
    public function setGameName($gameName)
    {
        $this->game_name = $gameName;

        return $this;
    }

    /**
     * Get gameName
     *
     * @return string
     */
    public function getGameName()
    {
        return $this->game_name;
    }

    /**
     * Set systemId
     *
     * @param string $systemId
     *
     * @return Lineup
     */
    public function setSystemId($systemId)
    {
        $this->system_id = $systemId;

        return $this;
    }

    /**
     * Get systemId
     *
     * @return string
     */
    public function getSystemId()
    {
        return $this->system_id;
    }

    /**
     * Set systemName
     *
     * @param string $systemName
     *
     * @return Lineup
     */
    public function setSystemName($systemName)
    {
        $this->system_name = $systemName;

        return $this;
    }

    /**
     * Get systemName
     *
     * @return string
     */
    public function getSystemName()
    {
        return $this->system_name;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Lineup
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->members = new \Doctrine\Common\Collections\ArrayCollection();
        $this->offers = new ArrayCollection();
    }

    /**
     * Add member
     *
     * @param \App\Entity\User $member
     *
     * @return Lineup
     */
    public function addMember(\App\Entity\User $member)
    {
        $this->members[] = $member;

        return $this;
    }

    /**
     * Remove member
     *
     * @param \App\Entity\User $member
     */
    public function removeMember(\App\Entity\User $member)
    {
        $this->members->removeElement($member);
    }

    /**
     * Get members
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setLineUp($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getLineUp() === $this) {
                $offer->setLineUp(null);
            }
        }

        return $this;
    }
}
