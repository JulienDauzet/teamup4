<?php

/**
 * Auto generated by MySQL Workbench Schema Exporter.
 * Version 3.0.3 (doctrine2-annotation) on 2018-02-05 23:58:28.
 * Goto https://github.com/johmue/mysql-workbench-schema-exporter for more
 * information.
 */

namespace App\Entity;

use App\Entity\Follow;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Role\Role;
use Knojector\SteamAuthenticationBundle\User\AbstractSteamUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\User
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_user_email", columns={"email"})})
 * @UniqueEntity(fields="email", message="Ce mail est déjà utilisé")
 * @UniqueEntity(fields="pseudo", message="Ce pseudo est déjà utilisé")
 */
class User extends AbstractSteamUser implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $emailTemp;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    protected $emailToken;

    /**
     * not persisted plainPassword
     */
    private $plainPassword;

    /**
     * @ORM\Column(name="`password`", type="string", length=255)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File(mimeTypes={ "image/jpeg","image/gif","image/png" })
     */
    private $profilPic;

    /**
     * DC2Type:array
     *
     * @ORM\Column(name="roles", type="array")
     */
    protected $roles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $pseudo;


    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    protected $lostPasswordToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lostPasswordDate;

    /**
     * Non mapped field, used when changing the password
     */
    private $oldPassword;

    /**
     * Plusieurs utilisateurs sont liées à une lineUp
     * @ORM\ManyToOne(targetEntity="Lineup", inversedBy="members")
     * @ORM\JoinColumn(name="lineup_id", referencedColumnName="id")
     */
    protected $lineUp;

    /**
     *
     * @ORM\OneToOne(targetEntity="Team", mappedBy="owner")
     */
    private $team;

    /**
     * @var string
     *
     * @ORM\Column(name="discord", type="string", length=255, nullable=true)
     */
    private $discord;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube", type="string", length=255, nullable=true)
     */
    private $youtube;

    /**
     * @var string
     *
     * @ORM\Column(name="twitch", type="string", length=255, nullable=true)
     */
    private $twitch;

    /**
     * @var string
     *
     * @ORM\Column(name="bio", type="string", length=1255, nullable=true)
     */
    private $bio;


    /**
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="recep", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $notifications;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity="EgoGame", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $egoGames;

    /**
     * @ORM\OneToMany(targetEntity="PubNote", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $pubNotes;





    /**
     * Plusieurs utilisateurs ont plusieurs chats
     *
     * @ORM\ManyToMany(targetEntity="Chat", inversedBy="users")
     * @ORM\JoinTable(name="user_chat",
     * joinColumns={@ORM\JoinColumn(name="user_id",
    referencedColumnName="id", nullable=false, onDelete="CASCADE")},
     * inverseJoinColumns={@ORM\JoinColumn(name="chat_id",
    referencedColumnName="id", nullable=false, onDelete="CASCADE")}
     * )
     */
    protected $chats;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Follow", mappedBy="user",cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $follows;


    public function __construct()
    {
        $this->setRoles(['ROLE_USER']);
        $this->notifications = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->egoGames = new ArrayCollection();
        $this->pubNotes = new ArrayCollection();
        $this->chats = new ArrayCollection();
        $this->follows = new ArrayCollection();

    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \App\Entity\User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of email.
     *
     * @param string $email
     * @return \App\Entity\User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of password.
     *
     * @param string $password
     * @return \App\Entity\User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * Get the value of salt.
     *
     * @return string
     */
    public function getSalt()
    {
        // The bcrypt and argon2i algorithms don't require a separate salt.
        // You *may* need a real salt if you choose a different encoder.
        return null;
    }

    /**
     * Set the value of roles.
     *
     * @param string $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        $roles = [];
        foreach ($this->roles as $role) {
            $roles[] = new Role($role);
        }

        return $roles;
    }

    /**
     * Add a role
     *
     * @param string $role
     * @return User
     */
    public function addRole($role){
        $roles = $this->getRoles();
        if(!in_array($role, $roles)){
            $this->setRoles(array_merge($roles, array($role)));
        }

        return $this;
    }

    /**
     * Remove a role
     *
     * @param string $role
     * @return User
     */
    public function removeRole($role){
        $roles = $this->getRoles();
        if(in_array($role, $roles)){
            $this->setRoles(array_diff($roles, array($role)));
        }

        return $this;
    }

    /**
     * Set the value of lostPasswordToken.
     *
     * @param string $lostPasswordToken
     * @return \App\Entity\User
     */
    public function setLostPasswordToken($lostPasswordToken)
    {
        $this->lostPasswordToken = $lostPasswordToken;

        return $this;
    }

    /**
     * Get the value of lostPasswordToken.
     *
     * @return string
     */
    public function getLostPasswordToken()
    {
        return $this->lostPasswordToken;
    }

    /**
     * Set the value of lostPasswordDate.
     *
     * @param \DateTime $lostPasswordDate
     * @return \App\Entity\User
     */
    public function setLostPasswordDate($lostPasswordDate)
    {
        $this->lostPasswordDate = $lostPasswordDate;

        return $this;
    }

    /**
     * Get the value of lostPasswordDate.
     *
     * @return \DateTime
     */
    public function getLostPasswordDate()
    {
        return $this->lostPasswordDate;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            ) = unserialize($serialized);
    }

    /**
     * Set emailTemp
     *
     * @param string $emailTemp
     *
     * @return User
     */
    public function setEmailTemp($emailTemp)
    {
        $this->emailTemp = $emailTemp;

        return $this;
    }

    /**
     * Get emailTemp
     *
     * @return string
     */
    public function getEmailTemp()
    {
        return $this->emailTemp;
    }

    /**
     * Set emailToken
     *
     * @param string $emailToken
     *
     * @return User
     */
    public function setEmailToken($emailToken)
    {
        $this->emailToken = $emailToken;

        return $this;
    }

    /**
     * Get emailToken
     *
     * @return string
     */
    public function getEmailToken()
    {
        return $this->emailToken;
    }

    /**
     * Set oldPassword
     *
     * @param string $oldPassword
     *
     * @return User
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * Get oldPassword
     *
     * @return string
     */
    public function getOldPassword()
    {

        return $this->oldPassword;
    }

    /**
     * Set pseudo
     *
     * @param string $pseudo
     *
     * @return User
     */
    public function setPseudo($pseudo)
    {
        if(isset($this->profileName)){
            $this->pseudo = $this->profileName;
        }else{
            $this->pseudo = $pseudo;
        }


        return $this;
    }

    /**
     * Get pseudo
     *
     * @return string
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * Set lineUp
     *
     * @param \App\Entity\Lineup $lineUp
     *
     * @return User
     */
    public function setLineUp(\App\Entity\Lineup $lineUp = null)
    {
        $this->lineUp = $lineUp;

        return $this;
    }

    /**
     * Get lineUp
     *
     * @return \App\Entity\Lineup
     */
    public function getLineUp()
    {
        return $this->lineUp;
    }


    /**
     * Set profilPic
     *
     * @param string $profilPic
     *
     * @return User
     */
    public function setProfilPic($profilPic)
    {
        $this->profilPic = $profilPic;

        return $this;
    }

    /**
     * Get profilPic
     *
     * @return string
     */
    public function getProfilPic()
    {
        return $this->profilPic;
    }

    /**
     * Set team
     *
     * @param \App\Entity\Team $team
     *
     * @return User
     */
    public function setTeam(\App\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \App\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Add notification.
     *
     * @param \App\Entity\Notification $notification
     *
     * @return User
     */
    public function addNotification(\App\Entity\Notification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification.
     *
     * @param \App\Entity\Notification $notification
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNotification(\App\Entity\Notification $notification)
    {
        return $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setUserId($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getUserId() === $this) {
                $post->setUserId(null);
            }
        }

        return $this;
    }

    public function getDiscord(): ?string
    {
        return $this->discord;
    }

    public function setDiscord(?string $discord): self
    {
        $this->discord = $discord;

        return $this;
    }

    public function getYoutube(): ?string
    {
        return $this->youtube;
    }

    public function setYoutube(?string $youtube): self
    {
        $this->youtube = $youtube;

        return $this;
    }

    public function getTwitch(): ?string
    {
        return $this->twitch;
    }

    public function setTwitch(?string $twitch): self
    {
        $this->twitch = $twitch;

        return $this;
    }

    public function getBio(): ?string
    {
        return $this->bio;
    }

    public function setBio(?string $bio): self
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * @return Collection|EgoGame[]
     */
    public function getEgoGames(): Collection
    {
        return $this->egoGames;
    }

    public function addEgoGame(EgoGame $egoGame): self
    {
        if (!$this->egoGames->contains($egoGame)) {
            $this->egoGames[] = $egoGame;
            $egoGame->setUser($this);
        }

        return $this;
    }

    public function removeEgoGame(EgoGame $egoGame): self
    {
        if ($this->egoGames->contains($egoGame)) {
            $this->egoGames->removeElement($egoGame);
            // set the owning side to null (unless already changed)
            if ($egoGame->getUser() === $this) {
                $egoGame->setUser(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        // to show the name of the Category in the select
        if($this->profileName == null){
            return $this->pseudo;
        }else{
            return $this->profileName;
        }

        // to show the id of the Category in the select
        // return $this->id;
    }

    /**
     * @return Collection|PubNote[]
     */
    public function getPubNotes(): Collection
    {
        return $this->pubNotes;
    }

    public function addPubNote(PubNote $pubNote): self
    {
        if (!$this->pubNotes->contains($pubNote)) {
            $this->pubNotes[] = $pubNote;
            $pubNote->setUser($this);
        }

        return $this;
    }

    public function removePubNote(PubNote $pubNote): self
    {
        if ($this->pubNotes->contains($pubNote)) {
            $this->pubNotes->removeElement($pubNote);
            // set the owning side to null (unless already changed)
            if ($pubNote->getUser() === $this) {
                $pubNote->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Chat[]
     */
    public function getChats(): Collection
    {
        return $this->chats;
    }

    public function addChat(Chat $chat): self
    {
        if (!$this->chats->contains($chat)) {
            $this->chats[] = $chat;
        }

        return $this;
    }

    public function removeChat(Chat $chat): self
    {
        if ($this->chats->contains($chat)) {
            $this->chats->removeElement($chat);
        }

        return $this;
    }

    /**
     * @return Collection|Follow[]
     */
    public function getFollows(): Collection
    {
        return $this->follows;
    }

    public function addFollow(Follow $follow): self
    {
        if (!$this->follows->contains($follow)) {
            $this->follows[] = $follow;
            $follow->setUser($this);
        }

        return $this;
    }

    public function removeFollow(Follow $follow): self
    {
        if ($this->follows->contains($follow)) {
            $this->follows->removeElement($follow);
            // set the owning side to null (unless already changed)
            if ($follow->getUser() === $this) {
                $follow->setUser(null);
            }
        }

        return $this;
    }


}
