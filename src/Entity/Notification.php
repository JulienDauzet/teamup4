<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Plusieurs posts sont liées à un utilisateur
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="emett", referencedColumnName="id", nullable=true)
     */
    private $emett;

    /**
     * Plusieurs posts sont liées à un utilisateur
     * @ORM\ManyToOne(targetEntity="User", inversedBy="notification")
     * @ORM\JoinColumn(name="recep", referencedColumnName="id")
     */
    private $recep;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text")
     */
    private $type;

    /**
     * Plusieurs posts sont liées à un utilisateur
     * @ORM\ManyToOne(targetEntity="Lineup")
     * @ORM\JoinColumn(name="lineup", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $lineup;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->status = 0;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emett.
     *
     * @param int|null $emett
     *
     * @return Notification
     */
    public function setEmett($emett = null)
    {
        $this->emett = $emett;

        return $this;
    }

    /**
     * Get emett.
     *
     * @return int|null
     */
    public function getEmett()
    {
        return $this->emett;
    }

    /**
     * Set recep.
     *
     * @param int $recep
     *
     * @return Notification
     */
    public function setRecep($recep)
    {
        $this->recep = $recep;

        return $this;
    }

    /**
     * Get recep.
     *
     * @return int
     */
    public function getRecep()
    {
        return $this->recep;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return Notification
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Notification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Notification
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set lineup.
     *
     * @param int $lineup
     *
     * @return Notification
     */
    public function setLineup($lineup)
    {
        $this->lineup = $lineup;

        return $this;
    }

    /**
     * Get lineup.
     *
     * @return int
     */
    public function getLineup()
    {
        return $this->lineup;
    }
}
