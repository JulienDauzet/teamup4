<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\Offer;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Application;


class OfferController extends Controller
{
    /**
     * Lists all post entities.
     *
     * @Route("/offres", name="offres_de_recrutement", methods={"GET"})
     */
    public function Offres(Request $request,PaginatorInterface $paginator, EntityManagerInterface $em)
    {
        $offerRepository = $em->getRepository(Offer::class);
        if($request->query->get('query') || $request->query->get('poste')){
            $pagination = $paginator->paginate(
                $offerRepository->findByGameAndPoste($request->query->get('query'),$request->query->get('poste')), // La query que l'on veut paginer
                $request->query->getInt('page', 1), // On récupère le numéro de la page et on le défini à 1 par défaut
                20 // Nombre d'éléments affiché par page
            );
        }else {

            $pagination = $paginator->paginate(
                $offerRepository->findAll(), // La query que l'on veut paginer
                $request->query->getInt('page', 1), // On récupère le numéro de la page et on le défini à 1 par défaut
                20 // Nombre d'éléments affiché par page
            );
        }

        return $this->render('Offer/annuaire.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Finds and displays a post entity.
     *
     * @Route("offre/{id}", name="offer_show", methods={"GET", "POST"})
     */
    public function showAction(Offer $offer,Request $request){

        $application = new Application();
        $form = $this->createForm('App\Form\ApplicationType', $application);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $application->setUser($this->getUser());
            $application->setOffre($offer);

            $notification = new Notification();
            $notification->setEmett($this->getUser());
            $notification->setRecep($offer->getLineUp()->getTeam()->getOwner());
            $notification->setContent(
                $this->getUser()->getPseudo().$this->getUser()->getProfileName().
                " a candidaté à votre offre de recrutement pour le poste de ".
                $offer->getTitle().
                " sur ".
                $offer->getLineUp()->getGameName()
            );
            $notification->setType("application");

            $em = $this->getDoctrine()->getManager();
            $em->persist($notification);
            $em->persist($application);
            $em->flush();
            $this->addFlash(
                "success", "Votre candidature a bien été prise en compte."
            );

            return $this->redirectToRoute('offer_show', array('id' => $offer->getId()));
        }

        return $this->render('Offer/show.html.twig', array(
            'offer' => $offer,
            'form' => $form->createView()
        ));
    }

}
