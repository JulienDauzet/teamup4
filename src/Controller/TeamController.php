<?php

namespace App\Controller;

use App\Entity\Comment;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use App\Form\PostType;
use App\Entity\Post;
use App\Entity\Team;
use App\Form\TeamType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TeamController extends Controller
{
    /**
     * @Route("/team/create", name="create_team")
     */
    public function teamAction(Request $request, EntityManagerInterface $em)
    {

        $user = $this->getUser();
            if(!$user->getTeam()){
                $team = new Team();
                $teamForm = $this->createForm(TeamType::class, $team, array());
                $teamForm->handleRequest($request);
                if ($teamForm->isSubmitted() && $teamForm->isValid()) {
                    $team->setOwner($user);
                    $team->setBanner("");
                    $team->setLogo("");
                    $em->persist($team);
                    $em->flush();
                    $this->addFlash(
                        "success", "Votre structure est en ligne, vous pouvez d'ores et déjà l'administrer."
                    );
                    return $this->redirectToRoute('display_team', array('ID' => $team->getId()));

                }

                return $this->render('/Team/create.html.twig', array(
                    'teamForm' => $teamForm->createView()));
            }else{
                return $this->redirectToRoute('display_team', array('ID' => $user->getTeam()->getId()));
            }
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param AuthorizationCheckerInterface $authChecker
     * @param integer $ID
     * @return Response
     *
     * @Route("/team/display/{ID}", name="display_team")
     */
    public function display(ManagerRegistry $doctrine, AuthorizationCheckerInterface $authChecker, int $ID, Request $request, EntityManagerInterface $em):Response
    {
        $team = $doctrine->getRepository(Team::class)->findOneBy(['id' => $ID]);


        return $this->render('Team/display.html.twig', [
            'team' => $team
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param integer $ID
     * @return Response
     * @Route("/team/admin/{ID}", name="admin_team")
     */
    public function teamAdmin(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $em, int $ID):Response
    {
        $team = $doctrine->getRepository(Team::class)->findOneBy(['id' => $ID]);
        $oldLogo = $team->getLogo();
        $oldBanner = $team->getBanner();
        $user = $this->getUser();
        if($team->getOwner() == $user){
            $teamForm = $this->createForm(TeamType::class, $team, array());
            $teamForm->handleRequest($request);
            if ($teamForm->isSubmitted() && $teamForm->isValid()) {
                $banner = $team->getBanner();
                if($banner !== null) {
                    $bannerFileName = $this->generateUniqueFileName() . '.' . $banner->guessExtension();

                    try {
                        $banner->move(
                            $this->getParameter('team_banner_directory'),
                            $bannerFileName
                        );
                        $team->setBanner($bannerFileName);
                    } catch (FileException $e) {
                    }

                }else{
                    $team->setBanner($oldBanner);
                }
                $logo = $team->getLogo();
                if($logo !== null) {
                    $logoFileName = $this->generateUniqueFileName() . '.' . $logo->guessExtension();

                    try {
                        $logo->move(
                            $this->getParameter('team_logo_directory'),
                            $logoFileName
                        );
                        $team->setLogo($logoFileName);
                    } catch (FileException $e) {
                    }

                }else{
                    $team->setLogo($oldLogo);
                }
                $em->persist($team);
                $em->flush();
                $this->addFlash(
                    "success", "Les modifications effectuées sont enregistrées."
                );
                return $this->redirectToRoute('display_team', array('ID' => $team->getId()));

            }

            return $this->render('/Team/edit.html.twig', array(
                'teamForm' => $teamForm->createView(),
                'team' => $team
                ));
        }else{
            return $this->redirectToRoute('admin_team', array('ID' => $user->getTeam()->getId()));
        }
    }


    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}