<?php


namespace App\Controller\api;

use App\Service\Api\GameApiService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ApiController extends Controller
{
    /**
     * Retourne un array json contenant id du jeu, name et ses plateformes (osef de popularity, sert juste à trier)
     * @Route("/api/games/{query}")
     * @param String $query
     * @param GameApiService $gapi
     * @return JsonResponse
     */
    public function gamesAction($query, GameApiService $gapi)
    {
        $games = $gapi->getGameNameByNameApi($query, 10);
//        return new Response($games);
        return new JsonResponse($games);
    }

    /**
     * Retourne un array json contenant id du jeu et name (osef de popularity)
     * @Route("/api/cover/{id}")
     * @param integer $id
     * @param GameApiService $gapi
     * @return Response
     */
    public function gamecover($id, GameApiService $gapi)
    {
        $cover = $gapi->getCoverByIdApi($id);
//        return new Response($games);
        return new JsonResponse($cover);
    }

    /**
     * @Route("/api/system/{id}")
     * @param integer $id
     * @param GameApiService $gapi
     * @return JsonResponse
     */
    public function systemAction($id, GameApiService $gapi)
    {
        $system = $gapi->getSystemByIdApi($id);
        return new JsonResponse($system);
    }

    /**
     * @Route("/api/game/{id}")
     * @param integer $id
     * @param GameApiService $gapi
     * @return Response
     */
    public function gameAction($id, GameApiService $gapi)
    {
        var_dump($gapi->getFullGameById($id));
        echo ("-------------------------------------");
        var_dump($gapi->getGameById($id));
        return new Response();
    }

    /**
     * @Route("/api/gamenames/{query}")
     * @param String $query
     * @param GameApiService $gapi
     * @return Response
     */
    public function gameNamesAction($query, GameApiService $gapi)
    {
        var_dump($gapi->getGameNamesByQuery($query, 5));
        return new Response();
    }
}