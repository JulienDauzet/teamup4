<?php
namespace App\Controller;

use App\Entity\Follow;
use App\Entity\Notification;
use App\Entity\Offer;
use App\Entity\PubNote;
use App\Entity\User;
use App\Entity\Team;
use App\Entity\Message;
use App\Entity\Chat;
use App\Form\PubNoteType;
use Doctrine\Common\Persistence\ManagerRegistry;
use GuzzleHttp\Exception\GuzzleException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;
// Forms
use App\Form\ChangePasswordType;
use App\Form\ChangeEmailType;
use App\Form\UserInfoType;
// Services
use App\Service\Security\UserEmailService;
use App\Model\AppSteamGame;
use App\Entity\EgoGame;

// Note : il serait tout à fait possible de fusionner les 3 Actions (userInfoAction, userPasswordAction, userEmailAction) en une seule
class UserController extends Controller
{

    /**
     * @Route("/private/user", name="user")
     * 
     * @param Request $request
     * @param EntityManagerInterface $em
     * 
     * @return Response
     */
    public function userAction(Request $request, EntityManagerInterface $em)
    {
        $user = $this->getUser();
        $oldPic = $user->getProfilPic();
        $userInfoForm = $this->createForm(UserInfoType::class, $user, array());
        $userInfoForm->handleRequest($request);
        if ($userInfoForm->isSubmitted() && $userInfoForm->isValid()) {
            $profilePic = $user->getProfilPic();
            if($profilePic!== null){
                $fileName = $this->generateUniqueFileName().'.'.$profilePic->guessExtension();

                try {
                    $profilePic->move(
                        $this->getParameter('profil_pic_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                }

                $user->setProfilPic($fileName);
            }else{
                $user->setProfilPic($oldPic);
            }

            $em->flush();
            $this->addFlash(
                "success", "Modifications enregistrées avec succès."
            );
        }        
        
        return $this->render('/User/user.html.twig', array('userInfoForm' => $userInfoForm->createView()));
    }

    /**
     * Changer le mdp de l'utilisateur
     * 
     * @Route("/private/user/password", name="userpassword")
     * 
     * @param Request $request
     * @param EntityManagerInterface $em     
     * @param UserPasswordEncoderInterface $passwordEncoder
     * 
     * @return Response
     */
    public function userPasswordAction(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = $this->getUser();
        $changePasswordForm = $this->createForm(ChangePasswordType::class, $user, array());
        $changePasswordForm->handleRequest($request);
        if ($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()) {
            // Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);            
            // On sauvegarde l'utilisateur
            $em->flush();
            $this->addFlash(
                "success", "Mot de passe changé avec succès"
            );
        }

        return $this->render('/User/userpassword.html.twig', array('changePasswordForm' => $changePasswordForm->createView()));
    }

    /**
     * Changer l'email de l'utilisateur
     * 
     * @Route("/private/user/email", name="useremail")
     * 
     * @param Request $request
     * @param EntityManagerInterface $em     
     * @param UserEmailService $userEmailService
     * 
     * @return Response
     */
    public function userEmailAction(Request $request, EntityManagerInterface $em, UserEmailService $userEmailService)
    {
        $user = $this->getUser();
        $changeEmailForm = $this->createForm(ChangeEmailType::class, $user, array());
        $changeEmailForm->handleRequest($request);
        if ($changeEmailForm->isSubmitted() && $changeEmailForm->isValid()) {
            // On génère un token unique
            $emailToken = md5(uniqid());
            $user->setEmailToken($emailToken);
            // On sauvegarde l'utilisateur
            $em->flush();
            // On lui envoie un email de validation
            $userEmailService->sendValidationEmail($user);
            $this->addFlash(
                "warning", "Un email de validation vous a été envoyé"
            );
        }
        
        return $this->render('/User/useremail.html.twig', array('changeEmailForm' => $changeEmailForm->createView()));
    }

    /**
     * Changer le mdp de l'utilisateur
     *
     * @Route("/private/user/notification", name="usernotification")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function userNotification(Request $request, EntityManagerInterface $em, ManagerRegistry $doctrine)
    {
        $user = $this->getUser();
        $notifications = $user->getNotifications();

        foreach ($notifications as $notification) {
            if($notification->getStatus() == 0){
                $notification->setStatus(1);
                $em->persist($notification);
            }

        }
        $em->flush();

        return $this->render('/User/usernotification.html.twig', array('notifications' => $notifications));

    }

    /**
     * Finds and displays a post entity.
     *
     * @Route("/user/{id}", name="user_show", methods={"GET", "POST"})
     */
    public function showAction(User $user, Request $request, EntityManagerInterface $em, ManagerRegistry $doctrine)
    {   $youtube = null;
        $forms = [];
        $games = [];
        $allPosts = $this ->getDoctrine()->getRepository( 'App:Post' )->find($user);

        //USER a t il renseigné un youtube
        if($user->getYoutube() != null){
            $youtubeRaw = $user->getYoutube();
            //a t il renseigné l'url complete de son channel
            if(strpos($youtubeRaw, 'https://www.youtube.com/channel/UC') === 0){
                $youtube = str_replace("https://www.youtube.com/channel/UC", "http://www.youtube.com/embed/videoseries?list=UU", $youtubeRaw);//a t il renseigné l'url complete de son channel
            }
            //ou juste l'id de sa chaine
            else{
                $youtube = 'http://www.youtube.com/embed/videoseries?list='.str_replace("UC", "UU", $youtubeRaw);
            }
        }



        //creation des forms de notation *4
        $i=0;
        $formsViews = [];
        while($i<4){

            //$forms[$i]= $this->createForm('App\Form\PubNoteType');
            $forms[$i]= $this->container
                ->get('form.factory')
                ->createNamed('form' . $i, 'App\Form\PubNoteType');
            $forms[$i]->handleRequest($request);
            $formsViews[$i] = $forms[$i]->createView();

            if ($forms[$i]->isSubmitted() && $forms[$i]->isValid()) {
                //instancie la nouvelle note et l'hydrate
                $pubNote = new PubNote();
                $pubNote->setUser($this->getUser());
                $data = $forms[$i]->getData();
                $ego = $this ->getDoctrine()->getRepository( 'App:EgoGame' )->findBy(array('id' => $forms[$i]->get("egoGame")->getData()));
                $pubNote->setValue($data->getValue())
                    ->setEgoGame($ego[0]);

                //instancie la notif et l'hydrate
                $notification = new Notification();
                $notification->setEmett($this->getUser());
                $notification->setRecep($user);
                $notification->setContent(
                    $this->getUser()->getPseudo().$this->getUser()->getProfileName().
                    " vous a attribué la note de ".
                    $pubNote->getValue().
                    " à ".
                    $pubNote->getEgoGame()->getGameName()
                );
                $notification->setType("notation");





                $em = $this->getDoctrine()->getManager();
                $em->persist($notification);
                $em->persist($pubNote);
                $em->flush();
                $this->addFlash(
                    "success", "Votre note a été prise en compte."
                );
                return $this->redirectToRoute('user_show', array('id' => $user->getId()));
            }
            $i++;

        }



        if($user->getSteamId() != 0){
            try{
                $client = new Client(['base_uri' => 'http://api.steampowered.com/']);
                $response = $client->request('GET', 'IPlayerService/GetOwnedGames/v0001/?key=0BC59A43CDE71C8EB9A74D300F3E3B13&steamid='.$user->getSteamId().'&format=json&include_appinfo=1&include_played_free_games=1')
                    ->getBody()
                    ->getContents();
                $gamesRaw = json_decode($response, true);

                if(!empty($gamesRaw['response'])){
                    foreach($gamesRaw['response']['games'] as $gameRaw){
                        $game = new AppSteamGame();
                        $game->setAppid($gameRaw['appid'])
                            ->setName($gameRaw['name'])
                            ->setImgIconUrl($gameRaw['img_icon_url'])
                            ->setImgLogoUrl($gameRaw['img_logo_url'])
                            ->setPlaytimeForever($gameRaw['playtime_forever'])
                            ->setPlaytimeForeverMins();


                            if(isset($gameRaw['has_community_visible_stats'])){
                                $game->setHasCommunityVisibleStats($gameRaw['has_community_visible_stats']);
                            }


                        array_push($games, $game);
                    }
                    uasort($games, array($this, "cmp"));
                }


            }catch (GuzzleException $e) {
                echo("Erreur : API Steam erreur");
                echo($e->getMessage());
            }


        }

        return $this->render('User/show.html.twig', array(
            'user' => $user,
            'posts' => $allPosts,
            'youtube' => $youtube,
            'games' => $games,
            'forms' => $formsViews
        ));
    }

    /**
     * Creates a new egoGame entity.
     *
     * @Route("/new_egoGame", name="egogame_new", methods={"GET", "POST"})     *
     * @return Response
     */
    public function newAction(Request $request):Response
    {
        $user = $this->getUser();

        $egoGame = new egoGame();
        $egoGame->setUser($user);

        $form = $this->createForm('App\Form\EgoGameType', $egoGame);
        $form->handleRequest($request);
        $data = $form->getData();
        $egoGames_list = $this ->getDoctrine()->getRepository( 'App:EgoGame' )->findBy(array('user' => $user));


        if ($form->isSubmitted() && ($data->getGameId() == null || $data->getGameName() == null || $data->getOwnNote() == null )){
            $this->addFlash('warning', 'Vous devez remplir entièrement les champs !');
            return $this->redirectToRoute('egogame_new');
        }
        if ($form->isSubmitted() && $form->isValid()) {

            foreach($egoGames_list as $egoGamePresent){
                if($egoGamePresent->getGameId() == $data->getGameId()){

                    $this->addFlash(
                        "warning", "Vous vous êtes déjà attribués une note pour ".$data->getGameName()."."
                    );
                    return $this->redirectToRoute('egogame_new');

                }
            }

            $data = $form->getData();
            $egoGame->setGameId($data->getGameId());
            $egoGame->setGameName($data->getGameName());
            $egoGame->setImage($data->getImage());
            $egoGame->setOwnNote($data->getOwnNote());
            $em = $this->getDoctrine()->getManager();
            $em->persist($egoGame);
            $em->flush();
            $this->addFlash(
                "success", $data->getGameName()." ajouté à votre liste de prétentieux avec succès."
            );
            return $this->redirectToRoute('egogame_new');
        }

        return $this->render('egogame/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new egoGame entity.
     *
     * @Route("/add_pubnote/{id}", name="egogame_pubnote", methods={"GET", "POST"})     *
     * @return Response
     */
    public function addPubNote(Request $request, int $id):Response
    {
        $user = $this->getUser();

        $egoGame = $this ->getDoctrine()->getRepository( 'App:EgoGame' )->findBy(array('id' => $id));
        $pubNote = new PubNote();
        $pubNote->setUser($user)->setEgoGame($egoGame);


        $form = $this->createForm('App\Form\EgoGameType', $egoGame);
        $form->handleRequest($request);
        $data = $form->getData();
        $egoGames_list = $this ->getDoctrine()->getRepository( 'App:EgoGame' )->findBy(array('user' => $user));


        if ($form->isSubmitted() && ($data->getGameId() == null || $data->getGameName() == null || $data->getOwnNote() == null )){
            $this->addFlash('warning', 'Vous devez remplir entièrement les champs !');
            return $this->redirectToRoute('egogame_new');
        }
        if ($form->isSubmitted() && $form->isValid()) {

            foreach($egoGames_list as $egoGamePresent){
                if($egoGamePresent->getGameId() == $data->getGameId()){

                    $this->addFlash(
                        "warning", "Vous vous êtes déjà attribués une note pour ".$data->getGameName()."."
                    );
                    return $this->redirectToRoute('egogame_new');

                }
            }

            $data = $form->getData();
            $egoGame->setGameId($data->getGameId());
            $egoGame->setGameName($data->getGameName());
            $egoGame->setImage($data->getImage());
            $egoGame->setOwnNote($data->getOwnNote());
            $em = $this->getDoctrine()->getManager();
            $em->persist($egoGame);
            $em->flush();
            $this->addFlash(
                "success", $data->getGameName()." ajouté à votre liste de prétentieux avec succès."
            );
            return $this->redirectToRoute('egogame_new');
        }

        return $this->render('egogame/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Lists all post entities.
     *
     * @Route("/annuaire_joueurs", name="annuaire_joueurs", methods={"GET"})
     */
    public function AnnuaireJ(Request $request,PaginatorInterface $paginator, EntityManagerInterface $em)
    {
        $userRepository = $em->getRepository(User::class);
        if($request->query->get('queryUserPseudo') || $request->query->get('queryUserGame')){
            $pagination = $paginator->paginate(
                $userRepository->findByPseudoAndGame($request->query->get('queryUserPseudo'),$request->query->get('queryUserGame')), // La query que l'on veut paginer
                $request->query->getInt('page', 1), // On récupère le numéro de la page et on le défini à 1 par défaut
                20 // Nombre d'éléments affiché par page
            );
        }else {

            $pagination = $paginator->paginate(
                $userRepository->findAll(), // La query que l'on veut paginer
                $request->query->getInt('page', 1), // On récupère le numéro de la page et on le défini à 1 par défaut
                20 // Nombre d'éléments affiché par page
            );
        }

        return $this->render('Dashboard/annuaire_joueurs.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Lists all post entities.
     *
     * @Route("/annuaire_equipes", name="annuaire_equipes", methods={"GET"})
     */
    public function AnnuaireE(Request $request,PaginatorInterface $paginator, EntityManagerInterface $em)
    {
        $teamRepository = $em->getRepository(Team::class);
        if($request->query->get('queryTeamName') || $request->query->get('queryTeamGame')){
            $pagination = $paginator->paginate(
                $teamRepository->findByNameAndGame($request->query->get('queryTeamName'),$request->query->get('queryTeamGame')), // La query que l'on veut paginer
                $request->query->getInt('page', 1), // On récupère le numéro de la page et on le défini à 1 par défaut
                10 // Nombre d'éléments affiché par page
            );
        }else {

            $pagination = $paginator->paginate(
                $teamRepository->findAll(), // La query que l'on veut paginer
                $request->query->getInt('page', 1), // On récupère le numéro de la page et on le défini à 1 par défaut
                10 // Nombre d'éléments affiché par page
            );
        }

        return $this->render('Dashboard/annuaire_equipes.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Lists all post entities.
     *
     * @Route("/messagerie", name="messagerie", methods={"GET"})
     */
    public function Messagerie(Request $request,PaginatorInterface $paginator, EntityManagerInterface $em)
    {


        return $this->render('Messagerie/messagerie.html.twig', array(

        ));
    }

    /**
     * Lists all post entities.
     *
     * @Route("/kick/{id}", name="kick", methods={"GET"})
     */
    public function Kick(Request $request,PaginatorInterface $paginator, EntityManagerInterface $em, int $id)
    {
        $referer = $request->headers->get('referer');
        $userKick = $this->getDoctrine()->getRepository( 'App:User' )->findBy(array('id' => $id))[0];
        $userKick->setLineUp(null);
        $em = $this->getDoctrine()->getManager();
        $em->persist($userKick);

        $notif = new Notification();
        $notif->setRecep($userKick);
        $notif->setEmett($this->getUser());
        $notif->setContent($this->getUser()->getPseudo().$this->getUser()->getProfileName()." vous a exclu de votre line-up.    
        ");
        $notif->setType("kick");


        $em->persist($notif);



        $em->flush();

        $this->addFlash(
            "success", $userKick->getProfileName().$userKick->getPseudo()." a bien été exclu."
        );

        return $this->redirect($referer);



    }

    /**
     * Lists all post entities.
     *
     * @Route("/messagerie/{id}", name="chat", methods={"GET"})
     */
    public function Chat(Request $request,PaginatorInterface $paginator, EntityManagerInterface $em, int $id)
    {
        $userAuth = $this->getUser();
        $userMess = $this->getDoctrine()->getRepository( 'App:User' )->findBy(array('id' => $id));
        $isChat = false;

        //persistent collection des chats de l'utilisateur loggé
        $chatsUserAuth = $userAuth->getChats()->getValues();
        foreach($chatsUserAuth as $chatUserAuthRaw){
            //existe t il une instance de chat entre les deux users
            if(in_array($userMess[0],$chatUserAuthRaw->getUsers()->getValues())){
                $idChatExistant = $chatUserAuthRaw->getId();

            }

        }
        if(isset($idChatExistant)){

            return $this->redirectToRoute('chat_show', array('id' => $idChatExistant));

        }else{
            $chat = new Chat();
            $chat->addUser($userAuth);
            $chat->addUser($userMess[0]);
            $em = $this->getDoctrine()->getManager();
            $em->persist($chat);
            $em->flush();
            return $this->redirectToRoute('chat_show', array('id' => $chat->getId()));
        }
    }

    /**
     * Lists all post entities.
     *
     * @Route("/messages/{id}", name="chat_show", methods={"GET", "POST"})
     */
    public function ChatShow(Request $request,PaginatorInterface $paginator, EntityManagerInterface $em, $id)
    {
        $chat =  $this->getDoctrine()->getRepository( 'App:Chat' )->findOneBy(array('id' => $id));

        $messages = $chat->getMessages();

        foreach ($messages as $message) {
            if($message->getStatus() == 0 and $message->getUser() != $this->getUser()){
                $message->setStatus(1);
                $em->persist($message);
            }
        }
        $em->flush();



        $form = $this->createForm('App\Form\MessageType');
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $message = new Message();
            $message->setUser($this->getUser());
            $message->setChat($chat);
            $message->setStatus(0);
            $message->setContent($form->getData()->getContent());
            $em->persist($message);
            $em->flush();
            return $this->redirectToRoute('chat_show', array('id' => $chat->getId()));
        }

        return $this->render('Messagerie/chat.html.twig', array(
            'chat' => $chat,
            'form' => $form->createView()
        ));
    }

    /**
     * Lists all post entities.
     *
     * @Route("/follow/{id}", name="follow", methods={"GET"})
     */
    public function Follow(Request $request,PaginatorInterface $paginator, EntityManagerInterface $em, int $id)
    {
        $userFollow =  $this->getDoctrine()->getRepository( 'App:User' )->findOneBy(array('id' => $id));
        $userAuth = $this->getUser();

        //derniere route
        $referer = $request->headers->get('referer');

        $follow = new Follow();
        $follow->setUser($userAuth);
        $follow->setFollowed($userFollow);
        $userAuth->addFollow($follow);

        $notif = new Notification();
        $notif->setRecep($userFollow);
        $notif->setEmett($userAuth);
        $notif->setContent($userAuth->getPseudo().$userAuth->getProfileName()." s'est abonné à votre contenu.    
        ");
        $notif->setType("follow");


        $em->persist($notif);
        $em->persist($userAuth);
        $em->flush();
        $this->addFlash(
            "success", " Vous vous êtes abonné à ".$userFollow->getProfileName().$userFollow->getPseudo()."."
        );

            return $this->redirect($referer);

    }

    /**
     * Lists all post entities.
     *
     * @Route("/unfollow/{id}", name="unfollow", methods={"GET"})
     */
    public function Unfollow(Request $request,PaginatorInterface $paginator, EntityManagerInterface $em, int $id)
    {
        $userFollow =  $this->getDoctrine()->getRepository( 'App:User' )->findOneBy(array('id' => $id));
        $userAuth = $this->getUser();

        $Follow =  $this->getDoctrine()->getRepository( 'App:Follow' )->findOneBy(array('user' => $userAuth,'followed' => $userFollow));

        //derniere route
        $referer = $request->headers->get('referer');


        $em->remove($Follow);
        $em->flush();
        $this->addFlash(
            "success", " Vous vous êtes désabonné de ".$userFollow->getProfileName().$userFollow->getPseudo()."."
        );

        return $this->redirect($referer);

    }


    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    function cmp($a, $b)
    {
        return $this->intcmp($a->playtime_forever, $b->playtime_forever);
    }

    function intcmp($a,$b) {
        if((int)$a == (int)$b)return 0;
        if((int)$a  > (int)$b)return 1;
        if((int)$a  < (int)$b)return -1;
    }

}
