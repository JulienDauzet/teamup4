<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class AdministrationController extends Controller
{
    
    /**
     * Page accessible seulement aux admins
     * 
     * @Route("/private/administration", name="administration")
     */
    public function administrationAction()
    {
        return $this->render('/Administration/administration.html.twig', array());
    }
    
    /**
     * Page accessible seulement aux admins, où sont listé tous les utilisateurs du site 
     * 
     * @Route("/private/administration/users", name="administrationusers")
     */
    public function administrationUsersAction()
    {
        return $this->render('/Administration/users.html.twig', array());
    }    

}
