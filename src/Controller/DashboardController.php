<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\EgoGame;
use App\Entity\Offer;
use App\Entity\Team;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use App\Form\PostType;
use App\Entity\Post;
use App\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class DashboardController extends Controller
{
    /**
     * @Route("/accueil", name="dashboard")
     */
    public function dashboardAction(Request $request, EntityManagerInterface $em)
    {
        $relatedOffers = null;
        $relatedUsers = null;
        $teamUser = null;
        $relatedUsersRecruit = null;
        $postRepository = $this->getDoctrine()->getRepository( 'App:Post' );
        $user = $this->getUser();
        if($user){
            //posts a afficher
            $postToShow = new ArrayCollection();
            $followedUsers = $user->getFollows()->toArray();
            foreach($followedUsers as $follow){

                foreach($postRepository->findBy(['user' => $follow->getFollowed()]) as $cleanPost){

                    $postToShow->add($cleanPost);
                }

            }
            foreach($user->getPosts() as $userPost){
                $postToShow->add($userPost);
            }

            $iterator = $postToShow->getIterator();

            $iterator->uasort(function ($a, $b) {
                return ($a->getCreatedAt() > $b->getCreatedAt()) ? -1 : 1;
            });
            $postToShowSorted = new \Doctrine\Common\Collections\ArrayCollection(iterator_to_array($iterator));

            //cherche si user a egogame
            $userEgoGames = $this->getDoctrine()->getRepository(EgoGame::class)->findBy(['user' => $user]);
            if(!empty($userEgoGames)){
                $firstUserEgoGame = $userEgoGames[0]->getGameName();
                $relatedOffers = $this->getDoctrine()->getRepository(Offer::class)->findByGame($firstUserEgoGame)->getResult();
                $relatedUsers = $this->getDoctrine()->getRepository(User::class)->findByGame($firstUserEgoGame)->getResult();
                foreach($relatedUsers as $key => $relatedUser){
                    if($relatedUser == $user){
                        \array_splice($relatedUsers, $key, 1);
                    }
                }
            }
            $teamQueryUser = $this->getDoctrine()->getRepository(Team::class)->findByOwner($user)->getResult();
            if(!empty($teamQueryUser)){
                $teamUser = $teamQueryUser[0];
                if($teamUser->getLineUps()[0] != null){
                $lineUpGameTeamUser = $teamUser->getLineUps()[0]->getGameName();
                $relatedUsersRecruit = $this->getDoctrine()->getRepository(User::class)->findByGame($lineUpGameTeamUser)->getResult();
                foreach($relatedUsersRecruit as $key => $relatedUser){
                    if($relatedUser == $user){
                        \array_splice($relatedUsersRecruit, $key, 1);
                    }
                }
                }
            }
        }



        if(!isset($postToShowSorted)){
            return $this->redirectToRoute('register');
        }

        $post = new Post();
        $post->setUser($user);
        $postForm = $this->createForm(PostType::class, $post, array());
        $postForm->handleRequest($request);
        if ($postForm->isSubmitted() && $postForm->isValid()) {
                $media = $post->getMedia();
            if($media !== null){
                $fileName = $this->generateUniqueFileName() . '.' . $media->guessExtension();

                try {
                    $media->move(
                        $this->getParameter('post_media_directory'),
                        $fileName
                    );
                } catch (FileException $e) {
                }

                $post->setMedia($fileName);
            }else{
                $post->setMedia("");
            }

            $em->persist($post);
            $em->flush();
            $this->addFlash(
                "success", "Votre publication est en ligne."
            );
            $allPosts = $postRepository->findAll();
            unset($post);
            unset($postForm);
            $post = new Post();
            $post->setUser($user);
            $postForm = $this->createForm(PostType::class, $post, array());
        }

        return $this->render('/Dashboard/dashboard.html.twig', array(
            'postForm' => $postForm->createView(),
            'relatedOffers' => $relatedOffers,
            'relatedUsers' => $relatedUsers,
            'relatedUsersRecruit' => $relatedUsersRecruit,
            'teamUser' => $teamUser,
            'postToShow' => $postToShowSorted));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param AuthorizationCheckerInterface $authChecker
     * @param integer $ID
     * @return Response
     *
     * @Route("/post/delete/{ID}", name="post.delete")
     */
    public function delete(ManagerRegistry $doctrine, AuthorizationCheckerInterface $authChecker, int $ID):Response
    {
        if (false === $authChecker->isGranted('ROLE_USER')) {
            $this->addFlash('danger', 'Vous devez être connecté pour supprimer votre publication !');
            return $this->redirectToRoute('home');
        }

        $em = $doctrine->getManager();
        $repo = $doctrine->getRepository(Post::class);
        $postToDelete = $repo->find($ID);
        $em->remove($postToDelete);
        $em->flush();

        $this->addFlash('success', "Votre publication a bien été supprimée.");

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param AuthorizationCheckerInterface $authChecker
     * @param integer $ID
     * @return Response
     *
     * @Route("/annonce/{ID}", name="post.display")
     */
    public function display(ManagerRegistry $doctrine, AuthorizationCheckerInterface $authChecker, int $ID, Request $request, EntityManagerInterface $em):Response
    {
        $post = $doctrine->getRepository(Post::class)->findOneBy(['id' => $ID]);
        $user = $this->getUser();
        $comment = new Comment();
        $comment->setUser($user);
        $comment->setPost($post);
        $commentForm = $this->createForm(CommentType::class, $comment, array());
        $commentForm->handleRequest($request);
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {


            $em->persist($comment);
            $em->flush();
            $this->addFlash(
                "success", "Votre réponse à cette publication est en ligne."
            );
            unset($comment);
            unset($commentForm);
            $comment = new Comment();
            $comment->setUser($user);
            $comment->setPost($post);
            $commentForm = $this->createForm(CommentType::class, $comment, array());
        }

        return $this->render('Dashboard/display.html.twig', [
            'post' => $post,
            'commentForm' => $commentForm->createView()
        ]);
    }

}
