<?php

namespace App\Controller;

use App\Entity\Lineup;
use App\Entity\Notification;
use App\Entity\Team;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Offer;
/**
 * Lineup controller.
 *
 * @Route("lineup")
 */
class LineupController extends Controller
{
    /**
     * Lists all lineup entities.
     *
     * @Route("/", name="lineup_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $lineups = $em->getRepository('AppBundle:Lineup')->findAll();

        return $this->render('lineup/index.html.twig', array(
            'lineups' => $lineups,
        ));
    }

    /**
     * Creates a new lineup entity.
     *
     * @Route("/new", name="lineup_new", methods={"GET", "POST"})     *
     * @return Response
     */
    public function newAction(Request $request):Response
    {
        $user = $this->getUser();
        $team = $user->getTeam();
        $lineup = new Lineup();
        $lineup->setTeam($team);
        $form = $this->createForm('App\Form\LineupType', $lineup);
        $form->handleRequest($request);
        $data = $form->getData();
        if ($form->isSubmitted() && ($data->getGameId() == null || $data->getSystemId() == null || $data->getGameName() == null || $data->getSystemName() == null)){
            $this->addFlash('warning', 'Vous devez remplir entièrement les champs !');
            return $this->redirectToRoute('lineup_new');
        }
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $lineup->setGameId($data->getGameId());
            $lineup->setSystemId($data->getSystemId());
            $lineup->setGameName($data->getGameName());
            $lineup->setSystemName($data->getSystemName());
            $lineup->setImage($data->getImage());

            $em = $this->getDoctrine()->getManager();
            $em->persist($lineup);
            $em->flush();

            return $this->redirectToRoute('display_team', array('ID' => $team->getId()));
        }

        return $this->render('lineup/new.html.twig', array(
            'lineup' => $lineup,
            'team' => $team,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a lineup entity.
     *
     * @Route("/{ID}", name="lineup_show", methods={"GET", "POST"})
     */
    public function showAction(int $ID, Request $request, ManagerRegistry $doctrine, PaginatorInterface $paginator, EntityManagerInterface $em)
    {
        $lineup = $doctrine->getRepository(Lineup::class)->findOneBy(['id' => $ID]);
        $deleteForm = $this->createDeleteForm($lineup);

        $offer = new Offer();
        $form = $this->createForm('App\Form\OfferType', $offer);
        $form->handleRequest($request);

        //chercher user

        $userRepository = $em->getRepository(User::class);
        if($request->query->get('query')){
            $pagination = $paginator->paginate(
                $userRepository->findByPseudo($request->query->get('query')), // La query que l'on veut paginer
                $request->query->getInt('page', 1), // On récupère le numéro de la page et on le défini à 1 par défaut
                8 // Nombre d'éléments affiché par page
            );
        }else {

            $pagination = $paginator->paginate(
                $userRepository->findNotTeamed(), // La query que l'on veut paginer
                $request->query->getInt('page', 1), // On récupère le numéro de la page et on le défini à 1 par défaut
                8 // Nombre d'éléments affiché par page
            );
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $offer->setLineUp($lineup);

            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();
            $this->addFlash('success', 'Votre offre de recrutement est en ligne.');
            return $this->redirectToRoute('lineup_show', array('ID' => $lineup->getId()));
        }

        return $this->render('lineup/show.html.twig', array(
            'lineup' => $lineup,
            'delete_form' => $deleteForm->createView(),
            'pagination' => $pagination,
            'form' => $form->createView()
        ));
    }

    /**
     *
     * @Route("/{ID}/invit/{ID_lineup}", name="send_invit", methods={"GET"})
     */
    public function sendInvit(int $ID, int $ID_lineup, EntityManagerInterface $em, ManagerRegistry $doctrine):Response
    {
        $lineup = $doctrine->getRepository(Lineup::class)->findOneBy(['id' => $ID_lineup]);
        $user_emet = $this->getUser();
        $user_recep = $doctrine->getRepository(User::class)->findOneBy(['id' => $ID]);




        $notification = new Notification();
        $notification->setEmett($user_emet);
        $notification->setRecep($user_recep);
        $notification->setContent(
            $user_emet->getPseudo().$user_emet->getProfileName().
            " vous a invité à rejoindre la line-up ".
            $lineup->getGameName().
            " de la team ".
            $lineup->getTeam()->getName()
        );
        $notification->setType("invitation");
        $notification->setLineup($lineup);

        $em = $this->getDoctrine()->getManager();
        $em->persist($notification);
        $em->flush();
        $this->addFlash('success', 'Votre invitation a bien été envoyée.');
        return $this->redirectToRoute('lineup_show', ['lineup' => $lineup, 'ID' => $lineup->getId()]);
    }

    /**
     *
     * @Route("/{ID}/accept_invit/{ID_invit}/{ID_lineup}", name="accept_invit", methods={"GET"})
     */
    public function acceptInvit(int $ID, int $ID_lineup, int $ID_invit, EntityManagerInterface $em, ManagerRegistry $doctrine):Response
    {
        $lineup = $doctrine->getRepository(Lineup::class)->findOneBy(['id' => $ID_lineup]);
        $user_emet = $this->getUser();
        $user_recep = $doctrine->getRepository(User::class)->findOneBy(['id' => $ID]);
        $notif_invit = $doctrine->getRepository(Notification::class)->findOneBy(['id' => $ID_invit]);
        //on verifie que l'user n'ait pas deja de team
        $users_non_teamed = $doctrine->getRepository(User::class)->findNotTeamed()->getResult();

        if(in_array($user_emet, $users_non_teamed) && !empty($users_non_teamed)){


            //on crée la notification d'acceptation
            $notification = new Notification();
            $notification->setEmett($user_emet);
            $notification->setRecep($user_recep);
            $notification->setContent(
                $user_emet->getPseudo().$user_emet->getProfileName().
                " a accepté votre invitation à rejoindre la line-up ".
                $lineup->getGameName().
                " de la team ".
                $lineup->getTeam()->getName()
            );
            $notification->setType("accept_invitation");

            //On crée le flashmessage qui confirme l'ajout a la line up
            $flashmessage = "Bienvenue dans la line-up ".
                $lineup->getGameName().
                " de la team ".
                $lineup->getTeam()->getName();

            //On set la lineup de l'user, on passe la notif en status 2 et on flush les entités
            $user_emet->setLineUp($lineup);
            $notif_invit->setStatus(2);
            $em = $this->getDoctrine()->getManager();
            $em->persist($notif_invit);
            $em->persist($notification);
            $em->persist($user_emet);
            $em->flush();

            $this->addFlash('success', $flashmessage);
            return $this->redirectToRoute('lineup_show', ['lineup' => $lineup, 'ID' => $lineup->getId()]);
        }

            $this->addFlash('warning', 'Vous faites déjà parti d\'une team, veuillez la quitter avant d\'en rejoindre une autre.');
            return $this->redirectToRoute('usernotification');
    }

    /**
     *
     * @Route("/{ID}/refuse_invit/{ID_invit}/{ID_lineup}", name="refuse_invit", methods={"GET"})
     */
    public function refuseInvit(int $ID, int $ID_lineup, int $ID_invit, EntityManagerInterface $em, ManagerRegistry $doctrine):Response
    {
        $lineup = $doctrine->getRepository(Lineup::class)->findOneBy(['id' => $ID_lineup]);
        $user_emet = $this->getUser();
        $user_recep = $doctrine->getRepository(User::class)->findOneBy(['id' => $ID]);
        $notif_invit = $doctrine->getRepository(Notification::class)->findOneBy(['id' => $ID_invit]);

            //on crée la notification d'acceptation
            $notification = new Notification();
            $notification->setEmett($user_emet);
            $notification->setRecep($user_recep);
            $notification->setContent(
                $user_emet->getPseudo().$user_emet->getProfileName().
                " a décliné votre invitation à rejoindre la line-up ".
                $lineup->getGameName().
                " de la team ".
                $lineup->getTeam()->getName()
            );
            $notification->setType("refuse_invitation");

            //On crée le flashmessage qui confirme l'ajout a la line up
            $flashmessage = "Invitation refusée.";

            //On set la lineup de l'user, on passe la notif en status 2 et on flush les entités
            $notif_invit->setStatus(2);
            $em = $this->getDoctrine()->getManager();
            $em->persist($notif_invit);
            $em->persist($notification);
            $em->flush();

            $this->addFlash('success', $flashmessage);
            return $this->redirectToRoute('usernotification');


    }

    /**
     * Displays a form to edit an existing lineup entity.
     *
     * @Route("/{ID}/edit", name="lineup_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Lineup $lineup)
    {
        $deleteForm = $this->createDeleteForm($lineup);
        $editForm = $this->createForm('App\Form\LineupType', $lineup);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lineup_edit', array('id' => $lineup->getId()));
        }

        return $this->render('lineup/edit.html.twig', array(
            'lineup' => $lineup,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a lineup entity.
     *
     * @Route("/{id}", name="lineup_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Lineup $lineup)
    {
        $form = $this->createDeleteForm($lineup);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            foreach($lineup->getMembers() as $member){
                $em = $this->getDoctrine()->getManager();
                $member->setLineUp(null);
                $em->persist($member);
                $em->flush();
            }
            $em = $this->getDoctrine()->getManager();
            $em->remove($lineup);
            $em->flush();
        }

        return $this->redirectToRoute('display_team', array('ID' => $lineup->getTeam()->getId()));
    }

    /**
     * Creates a form to delete a lineup entity.
     *
     * @param Lineup $lineup The lineup entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Lineup $lineup)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lineup_delete', array('id' => $lineup->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
